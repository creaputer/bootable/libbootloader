# Copyright (C) 2023 The Android Open Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

load("@rules_rust//cargo:defs.bzl", "cargo_build_script")
load("@rules_rust//rust:defs.bzl", "rust_library")

CRATE_FEATURES = [
    "default",
    "proc-macro",
]

rust_library(
    name = "proc-macro2",
    srcs = glob(["**/*.rs"]),
    crate_features = CRATE_FEATURES,
    edition = "2021",
    visibility = ["//visibility:public"],
    deps = [
        ":proc-macro2_build_script",
        "@unicode-ident",
    ],
)

cargo_build_script(
    name = "proc-macro2_build_script",
    srcs = glob(["**/*.rs"]),
    crate_features = CRATE_FEATURES,
    crate_root = "build.rs",
    edition = "2021",
    visibility = ["//visibility:private"],
)
